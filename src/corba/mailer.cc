/*
 * Copyright (C) 2017-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/corba/mailer.hh"

#include "src/enum_conversions.hh"
#include "src/log.hh"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>

#include <string>

namespace Fred {
namespace Akm {
namespace Corba {

namespace {

inline std::string to_template_name(Fred::Akm::NotificationType value)
{
    switch (value)
    {
        case Fred::Akm::NotificationType::akm_notification_candidate_ok:
            return "akm_candidate_state_ok";

        case Fred::Akm::NotificationType::akm_notification_candidate_ko:
            return "akm_candidate_state_ko";

        case Fred::Akm::NotificationType::akm_notification_managed_ok:
            return "akm_keyset_update";
    }
    throw std::invalid_argument("value doesn't exist in Fred::Akm::NotificationType");
}

void enqueue(
        const IMailer::Header& _header,
        const DomainNotifiedStatus& _domain_notified_status,
        const std::string& _template_name,
        const Nameservice& _ns,
        const std::string& _ns_path_mailer)
{
    log()->debug("prepare template parameters for template \"{}\"", _template_name);
    std::map<std::string, std::string> template_parameters;
    template_parameters["domain"] = _domain_notified_status.domain.fqdn;
    template_parameters["zone"] = ".cz"; // TODO hardwired, get from domain.name
    template_parameters["datetime"] = to_template_string(_domain_notified_status.last_at);
    template_parameters["days_to_left"] = "7"; // TODO hardwired, get from config (notify_update_within_x_days)
    std::vector<std::string> keys;
    boost::split(keys, _domain_notified_status.serialized_cdnskeys, boost::is_any_of("|"));
    for (size_t i = 0; i < keys.size(); ++i)
    {
        template_parameters["keys." + std::to_string(i)] = keys[i];
    }
    for (const auto& template_parameter : template_parameters)
    {
        log()->debug("template_parameter[\"{}\"] = \"{}\"", template_parameter.first, template_parameter.second);
    }

    try
    {
        log()->info("sending notification to template_name \"{}\"", _template_name);

        ccReg::Mailer_var mailer = ccReg::Mailer::_narrow(_ns.resolve(_ns_path_mailer));

        ccReg::MailHeader header;
        const auto emails = boost::algorithm::join(_header.to, ", ");
        header.h_to = CORBA::string_dup(emails.c_str());
        if (_header.from)
        {
            header.h_from = CORBA::string_dup((*_header.from).c_str());
        }
        if (_header.reply_to)
        {
           header.h_reply_to = CORBA::string_dup((*_header.reply_to).c_str());
        }

        ccReg::KeyValues params;
        params.length(template_parameters.size());
        auto params_idx = 0;
        for (const auto& kv : template_parameters)
        {
            params[params_idx].key = CORBA::string_dup(kv.first.c_str());
            params[params_idx].value = CORBA::string_dup(kv.second.c_str());

            params_idx += 1;
        }

        CORBA::String_var dummy;
        mailer->mailNotify(
            CORBA::string_dup(_template_name.c_str()),
            header,
            params,
            ccReg::Lists(),
            ccReg::Attachment_seq(),
            false,
            dummy
        );
    }
    catch (const CORBA::SystemException &e)
    {
        throw std::runtime_error(e._name());
    }
    catch (const CORBA::Exception &e)
    {
        throw std::runtime_error(e._name());
    }
}

} // namespace Fred::Akm::Corba::{anonymous}

Mailer::Mailer(const Nameservice& _ns, const std::string& _ns_path_mailer)
    : ns_(_ns), ns_path_mailer_(_ns_path_mailer)
{
}

void Mailer::enqueue_akm_notification_candidate_ok(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const
{
    enqueue(_header,
            _domain_notified_status,
            to_template_name(Fred::Akm::NotificationType::akm_notification_candidate_ok),
            ns_,
            ns_path_mailer_);
}

void Mailer::enqueue_akm_notification_candidate_ko(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const
{
    enqueue(_header,
            _domain_notified_status,
            to_template_name(Fred::Akm::NotificationType::akm_notification_candidate_ko),
            ns_,
            ns_path_mailer_);
}

void Mailer::enqueue_akm_notification_managed_ok(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const
{
    enqueue(_header,
            _domain_notified_status,
            to_template_name(Fred::Akm::NotificationType::akm_notification_managed_ok),
            ns_,
            ns_path_mailer_);
}

} // namespace Fred::Akm::Corba
} // namespace Fred::Akm
} // namespace Fred
