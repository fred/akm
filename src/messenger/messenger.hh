/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MESSENGER_HH_33649A874E5A4F2FA0EDBFEB23F57D7F
#define MESSENGER_HH_33649A874E5A4F2FA0EDBFEB23F57D7F

#include "libhermes/libhermes.hh"

#include "src/i_mailer.hh"

#include <string>

namespace Fred {
namespace Akm {
namespace Messenger {

class Messenger : public IMailer
{
public:

    struct Configuration
    {
        std::string endpoint;
        bool archive;
        bool archive_rendered;
    };

    explicit Messenger(const Configuration& _configuration);

    virtual void enqueue_akm_notification_candidate_ok(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const override;

    virtual void enqueue_akm_notification_candidate_ko(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const override;

    virtual void enqueue_akm_notification_managed_ok(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const override;

private:
    Configuration configuration_;
};


} // namespace Fred::Akm::Messenger
} // namespace Fred::Akm
} // namespace Fred

#endif
