/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/messenger/messenger.hh"

#include "src/cdnskey.hh"
#include "src/log.hh"

#include "libhermes/libhermes.hh"

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

namespace Fred {
namespace Akm {
namespace Messenger {

namespace {

std::map<LibHermes::Email::RecipientEmail, std::set<LibHermes::Email::RecipientUuid>> to_libhermes_recipients(const std::vector<std::string>& _recipients)
{
    std::map<LibHermes::Email::RecipientEmail, std::set<LibHermes::Email::RecipientUuid>> result;
    for (const auto& recipient : _recipients)
    {
        result[LibHermes::Email::RecipientEmail{recipient}]; // create the record if does not exist yet
    }
    return result;
}

LibHermes::Email::SubjectTemplate get_libhermes_email_subject_template(const std::string& _mail_type)
{
    if (_mail_type == "akm_candidate_state_ok")
    {
        return LibHermes::Email::SubjectTemplate{"akm-candidate-state-ok-subject.txt"};
    }
    if (_mail_type == "akm_candidate_state_ko")
    {
        return LibHermes::Email::SubjectTemplate{"akm-candidate-state-ko-subject.txt"};
    }
    if (_mail_type == "akm_keyset_update")
    {
        return LibHermes::Email::SubjectTemplate{"akm-keyset-update-subject.txt"};
    }
    throw std::runtime_error{"unexpected _mail_type"};
}

LibHermes::Email::BodyTemplate get_libhermes_email_body_template(const std::string& _mail_type)
{
    if (_mail_type == "akm_candidate_state_ok")
    {
        return LibHermes::Email::BodyTemplate{"akm-candidate-state-ok-body.txt"};
    }
    if (_mail_type == "akm_candidate_state_ko")
    {
        return LibHermes::Email::BodyTemplate{"akm-candidate-state-ko-body.txt"};
    }
    if (_mail_type == "akm_keyset_update")
    {
        return LibHermes::Email::BodyTemplate{"akm-keyset-update-body.txt"};
    }
    throw std::runtime_error{"unexpected _mail_type"};
}

void enqueue(
        const IMailer::Header& _header,
        const Akm::DomainNotifiedStatus& _domain_notified_status,
        const std::string& _mail_type,
        const Messenger::Configuration& _messenger_configuration)
{
    try
    {
        log()->debug("prepare template parameters for template \"{}\"", _mail_type);

        const auto keys_context = [&]() {
            std::vector<LibHermes::StructValue> keys;
            const auto deserialized_keys = deserialize(_domain_notified_status.serialized_cdnskeys);
            std::transform(
                    deserialized_keys.begin(),
                    deserialized_keys.end(),
                    std::back_inserter(keys),
                    [](const auto& key) {
                            return LibHermes::StructValue{LibHermes::Struct{
                                    {LibHermes::StructKey{"flags"}, LibHermes::StructValue{key.flags}},
                                    {LibHermes::StructKey{"protocol"}, LibHermes::StructValue{key.proto}},
                                    {LibHermes::StructKey{"algorithm"}, LibHermes::StructValue{key.alg}},
                                    {LibHermes::StructKey{"key"}, LibHermes::StructValue{key.public_key}}}};
                    });
            return keys;
        }();

        const LibHermes::Struct template_parameters{
                {LibHermes::StructKey{"domain"}, LibHermes::StructValue{_domain_notified_status.domain.fqdn}},
                {LibHermes::StructKey{"zone"}, LibHermes::StructValue{std::string{".cz"}}}, // TODO hardwired, get from domain.name
                {LibHermes::StructKey{"datetime"}, LibHermes::StructValue{to_template_string(_domain_notified_status.last_at)}},
                {LibHermes::StructKey{"days_to_left"}, LibHermes::StructValue{std::string{"7"}}},
                {LibHermes::StructKey{"keys"}, LibHermes::StructValue{keys_context}}}; // TODO hardwired, get from config (notify_update_within_x_days)

        auto message_data =
                LibHermes::Email::make_minimal_email(
                        to_libhermes_recipients(_header.to),
                        get_libhermes_email_subject_template(_mail_type),
                        get_libhermes_email_body_template(_mail_type));
        if (_header.from != boost::none)
        {
            message_data.sender = LibHermes::Email::Sender{boost::algorithm::trim_copy(*_header.from)};
        }
        if (_header.reply_to != boost::none)
        {
            message_data.extra_headers[LibHermes::Email::ExtraHeaderKey{"Reply-To"}] =
                    LibHermes::Email::ExtraHeaderValue{boost::algorithm::trim_copy(*_header.reply_to)};
        }
        message_data.type = LibHermes::Email::Type{_mail_type};
        message_data.context = template_parameters;

        LibHermes::Connection<LibHermes::Service::EmailMessenger> connection{
                LibHermes::Connection<LibHermes::Service::EmailMessenger>::ConnectionString{
                        _messenger_configuration.endpoint}};

        LibHermes::Email::send(
                connection,
                message_data,
                LibHermes::Email::Archive{_messenger_configuration.archive},
                {});
    }
    catch (const LibHermes::Email::SendFailed& e)
    {
        log()->warn(boost::str(boost::format("gRPC exception caught while sending email: gRPC error code: %1%, error message: %2%, grpc_message_json: %3%") % e.error_code() % e.error_message() % e.grpc_message_json()));
        throw std::runtime_error(e.what());
    }
    catch (const std::exception& e)
    {
        log()->warn(boost::str(boost::format("std::exception caught while sending email: %1%") % e.what()));
        throw std::runtime_error(e.what());
    }
    catch (...)
    {
        log()->warn("exception caught while sending email");
        throw std::runtime_error("sening email failed");
    }
}

} // namespace Fred::Akm::Messenger::{anonymous}

Messenger::Messenger(const Messenger::Configuration& _configuration)
    : configuration_(_configuration)
{
}

void Messenger::enqueue_akm_notification_candidate_ok(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const
{
    enqueue(_header,
            _domain_notified_status,
            "akm_candidate_state_ok",
            configuration_);
}

void Messenger::enqueue_akm_notification_candidate_ko(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const
{
    enqueue(_header,
            _domain_notified_status,
            "akm_candidate_state_ko",
            configuration_);
}

void Messenger::enqueue_akm_notification_managed_ok(
        const Header& _header,
        const DomainNotifiedStatus& _domain_notified_status) const
{
    enqueue(_header,
            _domain_notified_status,
            "akm_keyset_update",
            configuration_);
}

} // namespace Fred::Akm::Messenger
} // namespace Fred::Akm
} // namespace Fred
