/*
 * Copyright (C) 2017-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/cdnskey.hh"

#include <boost/range/adaptors.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>

#include <cstdio>
#include <string>
#include <vector>
#include <numeric>

namespace Fred {
namespace Akm {

namespace {

std::string quote(const std::string& str)
{
    return "\"" + str + "\"";
}

std::string quote(int value)
{
    return std::to_string(value);
}

// do not change this, used for deserialization
Cdnskey make_cdnskey(const std::string& _serialized_cdnskey)
{
    Cdnskey cdnskey;
    char public_key[2049];
    const auto parts_assigned =
            std::sscanf(
                    _serialized_cdnskey.c_str(),
                    "\[flags: %d, protocol: %d, algorithm: %d, key: \"%2048[^\"]\"]",
                    &cdnskey.flags,
                    &cdnskey.proto,
                    &cdnskey.alg,
                    public_key);
    if (parts_assigned == EOF || parts_assigned < 4)
    {
        throw std::runtime_error("parsing cdnskey from string failed");
    }

    cdnskey.public_key = std::string(public_key);
    return cdnskey;
}

} // namespace Fred::Akm::{anonymous}


// see "src/sqlite/storage.cc"

// do not change this, used for serialization
std::string to_string(const Cdnskey& _cdnskey)
{
    static const std::string delim = ", ";
    return std::string("[") +
           "flags: " + quote(_cdnskey.flags) + delim +
           "protocol: " + quote(_cdnskey.proto) + delim +
           "algorithm: " + quote(_cdnskey.alg) + delim +
           "key: " + quote(_cdnskey.public_key) +
           "]";
}


bool operator==(const Cdnskey& _lhs, const Cdnskey& _rhs)
{
    return
        _lhs.status == _rhs.status &&
        _lhs.flags == _rhs.flags &&
        _lhs.proto == _rhs.proto &&
        _lhs.alg == _rhs.alg &&
        _lhs.public_key == _rhs.public_key;
}


bool operator!=(const Cdnskey& _lhs, const Cdnskey& _rhs)
{
    return !(_lhs == _rhs);
}

std::string serialize(const std::map<std::string, Cdnskey>& _cdnskeys)
{
    //boost::algorithm::join(neweset_domain_state->_cdnskeys | boost::adaptors::map_values, ","),
    const std::string delim = "|";
    const std::string result =
            std::accumulate(
                    _cdnskeys.begin(),
                    _cdnskeys.end(),
                    std::string(),
                    [delim](const std::string& s, const std::pair<const std::string, const Cdnskey&>& p)
                    {
                        return s + (s.empty() ? std::string() : delim) + to_string(p.second);
                    });
    return result;
}

std::vector<Cdnskey> deserialize(const std::string& _serialized_cdnskeys)
{
    std::vector<Cdnskey> keys;
    if (_serialized_cdnskeys.empty())
    {
        return keys;
    }
    std::vector<std::string> itemized_keys;
    boost::split(itemized_keys, _serialized_cdnskeys, boost::is_any_of("|"));
    std::transform(
            itemized_keys.begin(),
            itemized_keys.end(),
            std::back_inserter(keys),
            [](const auto& itemized_key) {
                return make_cdnskey(itemized_key);
            });
    return keys;
}

bool is_valid(const Cdnskey&)
{
    // we accept everything
    return true;
}

bool is_empty(const Cdnskey& _cdnskey)
{
    return _cdnskey.public_key.empty();
}

// RFC 8078 section 4. DNSSEC Delete Algorithm
bool is_deletekey(const Cdnskey& _cdnskey)
{
    // if (_cdnskey.alg == 0)
    // {
    //     return true;
    // }
    const std::string base64_encoded_zero = "AA==";
    if (_cdnskey.flags == 0 &&
        _cdnskey.proto == 3 &&
        _cdnskey.alg == 0 &&
        _cdnskey.public_key == base64_encoded_zero)
    {
        return true;
    }
    return false;
}

} //namespace Fred::Akm
} //namespace Fred
