/*
 * Copyright (C) 2017-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/notification.hh"

#include "src/enum_conversions.hh"
#include "src/log.hh"

namespace Fred {
namespace Akm {

void save_domain_status(
        const DomainNotifiedStatus& _domain_notified_status,
        const IStorage& _storage,
        const bool _dry_run)
{
    if (!_dry_run)
    {
        log()->debug("saving domain_status_notification of state \"{}\"", to_string(_domain_notified_status));
        _storage.set_domain_notified_status(_domain_notified_status);
    }
}

void notify_and_save_domain_status(
        const DomainNotifiedStatus& _domain_notified_status,
        const IStorage& _storage,
        const IAkm& _akm_backend,
        const IMailer& _mailer_backend,
        const bool _dry_run,
        const bool _fake_contact_emails)
{
    log()->info("shall notify to domain {}", _domain_notified_status.domain.fqdn);
    log()->debug("asking backend for emails for domain id {}", _domain_notified_status.domain.id);
    try
    {
        std::vector<std::string> tech_contacts = { "fake.contact.email.akm@example.com" };
        const bool real_contact_emails = !(_dry_run && _fake_contact_emails);
        if (real_contact_emails)
        {
            tech_contacts = _akm_backend.get_email_addresses_by_domain_id(_domain_notified_status.domain.id);
        }

        Fred::Akm::IMailer::Header header(tech_contacts);
        //log()->debug("will send to email(s): {}", header.to);

        if (!_dry_run)
        {
            switch (Conversion::Enums::to_notification_type(_domain_notified_status.domain_status))
            {
                case Fred::Akm::NotificationType::akm_notification_candidate_ok:
                    _mailer_backend.enqueue_akm_notification_candidate_ok(header, _domain_notified_status);
                    break;
                case Fred::Akm::NotificationType::akm_notification_candidate_ko:
                    _mailer_backend.enqueue_akm_notification_candidate_ko(header, _domain_notified_status);
                    break;
                case Fred::Akm::NotificationType::akm_notification_managed_ok:
                    _mailer_backend.enqueue_akm_notification_managed_ok(header, _domain_notified_status);
                    break;
            }
            // TODO (exceptions thrown by enqueue? (combination of "email sent + exception throw" would spam)
            save_domain_status(_domain_notified_status, _storage, _dry_run);
        }
    }
    catch (const ObjectNotFound& e)
    {
        log()->error(e.what());
        throw NotificationNotPossible();
    }
    catch (const AkmException& e)
    {
        log()->error(e.what());
        throw NotificationFailed();
    }
    catch (std::runtime_error& e)
    {
        log()->error(e.what());
        throw NotificationFailed();
    }
}

} // namespace Fred::Akm
} // namespace Fred
